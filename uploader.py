#!/usr/bin/python

from time import sleep
from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from os.path import expanduser, basename
from webbrowser import open_new_tab
from os import path
import json

from googleapi import *

# define global vars
fileNameLabel = descriptionScroll = privacy_option = video_category = video_file = selectLabel = selectBtn = uploadBtn = categoryLabel = categoryMenu = privacyLabel = privacyMenu = titleLabel = titleBox = descriptionLabel = descriptionBox = keywordsLabel = keywordsBox = None

def upload_callback(status, message):
  if status == 'uploading':
    # TODO: show progress bar
    pass
  elif status == 'upload_success':
    uploadBtn.config(text='Upload video')
    yesno = messagebox.askyesno('Success', '''
      Upload was successful.
      Do you want to open YouTube with your video?
      ''')
    if yesno:
      open_new_tab('https://youtube.com/watch?v=%s' % message)
  elif status == 'upload_unexpected_response':
    uploadBtn.config(text='Upload video')
    messagebox.showerror('Failure', 'The upload failed with an unexpected response: %s' % message)
  elif status == 'upload_max_retry_reached':
    uploadBtn.config(text='Upload video')
    messagebox.showerror('Error', 'No longer attempting to retry, aborting upload.')

def select_file_clicked():
  global video_file, fileNameLabel
  video_file = askopenfilename(initialdir=expanduser('~'), filetypes=[("Video files", ".mkv .mp4 .mpeg .webm .mov .flv .3gpp .wmv .avi .mpeg4 .mpegs"),("Any files", "*")], title='Select video file')
  if video_file:
    titleBox.delete(0, END)
    titleBox.insert(0, os.path.basename(video_file))
    fileNameLabel.config(text=video_file)

def upload_button_clicked():
  global video_file
  if video_file:
    if path.exists(video_file):
      youtube = get_authenticated_service()
      try:
        initialize_upload(youtube, {
          'title': titleBox.get(),
          'description': descriptionBox.get('1.0', END),
          'keywords': keywordsBox.get(),
          'category': video_category.get(),
          'file': video_file,
          'privacy': privacy_option.get()
        }, upload_callback)
      except HttpError as e:
        messagebox.showerror('HTTP error', 'An HTTP error %d occurred:\n%s' % (e.resp.status, re.sub('<[^<]+?>', '', json.loads(e.content)['error']['message'])))
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
    else:
      messagebox.showerror('Error', 'File doesn\'t exist')
  else:
    messagebox.showerror('Error', 'No file selected')

def setup_gui():
  global privacy_option, video_category, video_file, selectLabel, selectBtn, uploadBtn, categoryLabel, categoryMenu, privacyLabel, privacyMenu, titleLabel, titleBox, descriptionLabel, descriptionBox, keywordsLabel, keywordsBox, fileNameLabel
  window = Tk()
  window.title('MK\'s Simple YouTube Uploader')
  window.resizable(0, 0)
  #window.tk.call('wm', 'iconphoto', window._w, PhotoImage(file=path.join(os.path.dirname(sys.executable), 'app.png')))

  privacy_option = StringVar(window)
  privacy_option.set(VALID_PRIVACY_STATUSES[1])
  video_category = StringVar(window)
  video_category.set('Gaming')

  fileNameLabel = Label(window, text='', anchor='w')
  selectLabel = Label(window, text='Select a video file to upload:', anchor='w')
  selectBtn = Button(window, text='Select video file', command=select_file_clicked, width=20)
  uploadBtn = Button(window, text='>> Upload video <<', command=upload_button_clicked, width=40)
  categoryLabel = Label(window, text='Select the category for your video:', anchor='w')
  categoryMenu = OptionMenu(window, video_category, *list(VALID_VIDEO_CATEGORIES.keys()))
  categoryMenu.config(width=20)
  privacyLabel = Label(window, text='Select the privacy for your video:', anchor='w')
  privacyMenu = OptionMenu(window, privacy_option, *VALID_PRIVACY_STATUSES)
  privacyMenu.config(width=20)
  titleLabel = Label(window, text='Enter a title for your video:', anchor='w')
  titleBox = Entry(window, width=60, font=("Arial", 10))
  descriptionLabel = Label(window, text='Enter a description for your video:', anchor='nw')
  descriptionBox = Text(window, width=60, height=8, font=("Arial", 10))
  descriptionScroll = Scrollbar(window)
  descriptionScroll.config(command=descriptionBox.yview)
  descriptionBox.config(yscrollcommand=descriptionScroll.set)
  keywordsLabel = Label(window, text='Enter keywords (separated by commas):', anchor='w')
  keywordsBox = Entry(window, width=60, font=("Arial", 10))

  selectLabel.grid(row=0, column=0, sticky='w')
  selectBtn.grid(row=0, column=1, sticky='w')
  fileNameLabel.grid(row=1, column=1, sticky='w')
  categoryLabel.grid(row=2, column=0, sticky='w')
  categoryMenu.grid(row=2, column=1, sticky='w')
  privacyLabel.grid(row=3, column=0, sticky='w')
  privacyMenu.grid(row=3, column=1, sticky='w')
  titleLabel.grid(row=4, column=0, sticky='w')
  titleBox.grid(row=4, column=1, sticky='w')
  keywordsLabel.grid(row=5, column=0, sticky='w')
  keywordsBox.grid(row=5, column=1, sticky='w')
  descriptionLabel.grid(row=6, column=0, sticky='nw')
  descriptionBox.grid(row=6, column=1, sticky='w')
  descriptionScroll.grid(row=6, column=2, sticky='nsw')
  uploadBtn.grid(row=10, column=0, columnspan=3)
  window.mainloop()


if __name__ == '__main__':
  setup_gui()